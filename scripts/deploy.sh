BUCKET_NAME=$1
DISTRIBUTION_ID=$2

echo " -- install -- "
npm install

echo " -- build -- "
npm run-script build

echo " -- Deploy --"
aws s3 sync build s3://$BUCKET_NAME
aws clouldfront create-invalidation --distribuion-id $DISTRIBUTION_ID --paths "/*" --no-cli-pager